<div id="output"></div>


<div 
	style="	position:absolute; left:30%; width:40%; top:100px; border-style:solid; 
			border-width:1px; border-color:#000000; background:#ffffff; padding:10px;
			display:none;"
	id="modal"
>
	<form methods="post" id="modalForm">
		<input type="hidden" id="id" name="id" style="display:none;">
		<p>
			<label id="label_name">name</label>
			<input type="text" name="name" id="name" >
		</p>
		<p>
			<label id="label_email">email</label>
			<input type="text" name="email" id="email" >
		</p>
		<p>
			<label id="label_role">role</label>
			<br>
			<input type="radio" name="role" id="student" value="0">
			student
			<br>
			<input type="radio" name="role" id="teacher" value="1">
			teacher
			<br>
		</p>

		<p>
			<button type="button" onclick="saveData();" id="subutton"></button>
			<button type="button" onclick="jQuery('#modal').css('display','none');">cancel</button>
		</p>


	</form>
</div>

<script>

getData("users","read",{'name':'s','email':'e'});

function populateModal(view,cmd,data,show) {
	data = data[0];
	radio = {0:'student',1:'teacher'}
	for ( button in radio ) {
		if ( data.role == button ) jQuery("#"+radio[button]).prop('checked',true);
		else jQuery("#"+radio[button]).prop('checked',false);
	}
	jQuery("#name").val(data.name);
	jQuery("#email").val(data.email);
	jQuery("#id").val(data.id);
}	

function saveData() {
	var createorupdate = jQuery("#subutton").text();
	var id = jQuery("#id").val();
	var name = jQuery("#name").val();
	var email = jQuery("#email").val();
	var role = jQuery("input[name='role']:checked").val();
	if ( createorupdate === "update" ) {
		getAjax("users","update","",closeModal,{'name':name,'email':email,'role':role,'id':id});
	}
	else {
		getAjax("users","create","",closeModal,{'name':name,'email':email,'role':role});
	}
}



</script>



