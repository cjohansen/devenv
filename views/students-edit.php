<div id="output">
</div>

<div 
	style="	position:absolute; left:30%; width:40%; top:100px; border-style:solid; 
			border-width:1px; border-color:#000000; background:#ffffff; padding:10px;
			display:none;"
	id="modal"
>
	<form methods="post" id="modalForm">
		<input type="hidden" id="id" name="id" style="display:none;">
		<p>
			<label id="label_name">name</label>
			<input type="text" name="name" id="name" >
		</p>
		<p>
			<label id="label_description">description</label>
			<textarea name="description" id="description">
			</textarea>
		</p>
		<p>
			<label id="label_class">class</label>
			<select name="class_id" id="class_id">
			</select>
		</p>

		<p>
			<button type="button" onclick="saveData();" id="subutton"></button>
			<button type="button" onclick="jQuery('#modal').css('display','none');">cancel</button>
		</p>


	</form>
</div>


<script>


getData("students","read",{'name':'s','class':'t'});

var class_id;

function populateModal(view,cmd,data,show) {
	data = data[0];
	jQuery("#name").val(data.name);
	jQuery("#description").val(data.description);
	jQuery("#id").val(data.id);
	class_id = data.class_id;
	getAjax("classes","read","",populateClass,'');
}

function populateClass(view,cmd,data,show) {
	var selected = '';
	for ( cl in data ) {
		if ( class_id == data[cl].id ) selected = ' selected ';
		else selected = '';
		jQuery("#class_id").append("<option value='"+data[cl].id+"' "+selected+">"+data[cl].description);
	}
}

function saveData() {
	var createorupdate = jQuery("#subutton").text();
	var id = jQuery("#id").val();
	var name = jQuery("#name").val();
	var description = jQuery("#description").val();
	var class_id = jQuery("#class_id").val();
	if ( createorupdate === "update" ) {
		getAjax("students","update","",closeModal,{'name':name,'slug':name,'description':description,'id':id,'classes_id':class_id});
	}
	else {
		getAjax("students","create","",closeModal,{'name':name,'slug':name,'description':description,'classes_id':class_id});
	}
}


</script>



