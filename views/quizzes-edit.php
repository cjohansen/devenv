<div id="output"></div>

<div 
	style="	position:absolute; left:15%; width:70%; top:100px; border-style:solid; 
			border-width:1px; border-color:#000000; background:#ffffff; padding:10px;
			display:none; overflow-y: scroll; max-height: 500px;"
	id="modal"
>
	<form methods="post" id="modalForm">
		<input type="hidden" id="id" name="id" style="display:none;">
		<p>
			<label id="label_name">name</label>
			<input type="text" name="name" id="name" >
		</p>
		<p>
			<label id="label_description">description</label>
			<textarea name="description" id="description">
			</textarea>
		</p>
		<p>
			<label id="label_experience">experience</label>
			<input type="text" name="experience" id="experience" >
		</p>
		<p>
			<label id="label_sticky">sticky</label>
			<input type="checkbox" name="sticky" id="sticky" >
		</p>
		<p>
			<label id="label_media">media</label>
			<input type="text" name="media" id="media" >
		</p>
		<p>
			<label id="label_topic">topic</label>
			<select name="topic_id" id="topic_id">
			</select>
		</p>

		<div id="questionsanswers"></div>

		<p>
			<button type="button" onclick="saveData();">save</button>
			<button type="button" onclick="jQuery('#modal').css('display','none');">cancel</button>
		</p>

	</form>


</div>

<script>

getData("quizzes","read",{'name':'s','description':'t','topic':'s'});

var topic_id;

function populateTopic(view,cmd,data,show) {
	var selected = '';
	for ( topic in data ) {
		if ( topic_id == data[topic].id ) selected = ' selected ';
		else selected = '';
		jQuery("#topic_id").append("<option value='"+data[topic].id+"' "+selected+">"+data[topic].description);
	}
	getAjax("questions","read","",populateQuestions,{'id':14});		
}

function populateQuestions(view,cmd,data,show) {
	var output = '';
	for ( question in data ) {
		output += data[question].description+"<br>";
	}
	jQuery("#questionsanswers").append(output);
}

function populateModal(view,cmd,data,show) {
	data = data[0];
	jQuery("#name").val(data.name);
	jQuery("#description").val(data.description);
	jQuery("#id").val(data.id);
	//jQuery("#billede2").attr("src",data[0].description);	
	getAjax("topics","read","",populateTopic,'');	
}

function saveData() {
	var createorupdate = jQuery("#subutton").text();
	var id = jQuery("#id").val();
	var name = jQuery("#name").val();
	var description = jQuery("#description").val();
	if ( createorupdate === "update" ) {
		getAjax("topics","update","",closeModal,{'name':name,'slug':name,'description':description,'id':id});
	}
	else {
		getAjax("topics","create","",closeModal,{'name':name,'slug':name,'description':description});
	}
}


</script>

