<?php


class Pages {

	public function __construct() {
		add_action("admin_menu",[$this,"add_pages"]);
		add_action("admin_enqueue_scripts",[$this,"styles"]);
	}

	public function add_pages() {
		add_menu_page(	
			"quiz",
			"quiz",
			"usynlig",
			"quiz.php",
			function() {
				require_once("views/quiz-edit.php");
			},
			100
		);

		add_submenu_page(	
			"quiz.php",
			"classes",
			"classes",
			"quizb",
			"classes.php",
			function() {
				require_once("views/classes-edit.php");
			}
		);

		add_submenu_page(	
			"quiz.php",
			"topics",
			"topics",
			"quizb",
			"topics.php",
			function() {
				require_once("views/topics-edit.php");
			}
		);

		add_submenu_page(	
			"quiz.php",
			"students",
			"students",
			"quizb",
			"students.php",
			function() {
				require_once("views/students-edit.php");
			}
		);

		add_submenu_page(	
			"quiz.php",
			"quizzes",
			"quizzes",
			"quizb",
			"quizzes.php",
			function() {
				require_once("views/quizzes-edit.php");
			}
		);

		add_submenu_page(	
			"quiz.php",
			"dashboard",
			"dashboard",
			"quizb",
			"dashboard.php",
			function() {
				require_once("views/dashboard-edit.php");
			}
		);

		add_submenu_page(	
			"quiz.php",
			"users",
			"users",
			"quizb",
			"quiz-users.php",
			function() {
				require_once("views/users-edit.php");
			}
		);
	}

	public function styles() {
		$this->set_styles();
		$this->set_scripts();
	}

	private function set_styles() {
		wp_enqueue_style(	
			"test-main",
			Quiz::$url . "/assets/css/main.css",
			[],
			null,
			"screen"
		);
	}

	private function set_scripts() {
		wp_enqueue_script(	
			"script-main",
			"http://code.jquery.com/jquery-latest.min.js",
			[],
			null,
			false
		);

		wp_enqueue_script(	
			"script-main-2",
			Quiz::$url."/assets/js/main.js",
			[],
			null,
			false
		);

		wp_enqueue_script(	
			"script-main-3",
			"https://use.fontawesome.com/9e6517f4f2.js",
			[],
			null,
			false
		);		
	}
}

new Pages();





