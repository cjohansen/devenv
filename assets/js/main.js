var cmd;
var show;
var view;

function getData(view,cmd,show) {
	this.cmd = cmd;
	this.show = show;
	this.view = view;
	getAjax(view,cmd,show,addLines,'');
}

function getAjax(view,cmd,show,callback,data) {
	var request = new XMLHttpRequest();
	request.open("POST","http://localhost/wordpress_7/wp-admin/admin-ajax.php?action=quiz&cmd="+
		view+"-"+cmd, true);
	request.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	request.onreadystatechange = function () {
	  if (request.readyState != 4 || request.status != 200) return;
		callback(view,cmd,JSON.parse(request.responseText),show);
	};
  	var temp = new URLSearchParams();
	for (var key in data) {
	    temp.append(key,data[key]);
	}  	
	request.send(temp);
}

/*function getAjax(view,cmd,show,callback,data) {
	jQuery.ajax({
		url:'http://localhost/wordpress_7/wp-admin/admin-ajax.php?action=quiz&cmd='+view+'-'+cmd,
		//url:'http://localhost/wordpress_7/ajax/'+view+'-'+cmd,
		method:'post',
		data: data,
	    success:function(data) {
	    	console.log(data);
			callback(view,cmd,data,show);
	    },
	    error: function(error){
	        console.log(error);
	    }
	});	
}*/

function addLines(view,cmd,data,show) {
	var output = 	"<p><strong>"+view+"</strong>"+
					"<i class='fa fa-plus' onclick='createNewElement()' "+
					"style='margin-left:20px;' title='add new'></i>"+
					"</p><table class='widefat'><thead>";
	for ( field in show ) {
		output += "<td>"+field+"</td>"; 
	}
	output += "<td></td>";
	output += "</thead><tbody>";
	for ( line in data ) {
		output += "<tr >";
		for ( field in show ) {
			output += 	"<td onclick=\"editElement('"+view+"',"+data[line].id+
						",'"+show+"')\" >"+data[line][field]+"</td>"; 
		}
		output += 	"<td><i class='fa fa-trash' onclick=\"deleteElement('"+view+
					"',"+data[line].id+");\" title='delete'></i></td></tr>";
	}
	output += "</tbody>";
	jQuery("#output").html(output);
}

function editElement(view,data,show) {
	jQuery("#modal").css("display","inline-block");
	jQuery("#subutton").text("update");
	getAjax(view,"read-id",show,populateModal,{'id':data})	
}

function closeModal() {
	jQuery("#modal").css("display","none");
	reload();
}

function deleteElement(view,id) {
	getAjax(view,'delete','',reload,{'id':id});
}

function createNewElement() {
	jQuery("#modal").css("display","inline-block");
	jQuery("#subutton").text("create");
}

function reload() {
	getData(this.view,this.cmd,this.show);
}



