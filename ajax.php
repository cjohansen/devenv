<?php

class Ajax
{
	private $db;
	private $sqls = array(
		"users-read"			=>	"select * from users",
		"users-read-id"			=>	"select * from users where id=#id#n#",
		"users-update"			=>	"update users set name='#name#s#', email='#email#e#', role=#role#n# ".
									"where id=#id#n#",
		"topics-read"			=>	"select * from topics",
		"topics-read-id"		=>	"select * from topics where id=#id#n#",
		"topics-update"			=>	"update topics set name='#name#s#', description='#description#s#', slug='#slug#s#' ".
									"where id=#id#n#",
		"topics-create"			=>	"insert into topics(name,slug,description) values('#name#s#','#slug#s#','#description#s#')",
		"topics-delete"			=>	"delete from topics where id=#id#n#",
		"classes-read"			=>	"select * from classes",
		"classes-read-id"		=>	"select * from classes where id=#id#n#",
		"classes-update"		=>	"update classes set name='#name#s#', slug='#slug#s#', description='#description#s#' ".
									"where id=#id#n#",
		"classes-create"		=>	"insert into classes(name,slug,description) values('#name#s#','#slug#s#','#description#s#')",
		"classes-delete"		=>	"delete from classes where id=#id#n#",
		"students-read"			=>	"select s.id as id, s.name as name, c.description as class, s.classes_id as class_id ".
									"from students as s,classes as c where c.id=s.classes_id",
		"students-read-id"		=>	"select s.id as id, s.name as name, c.description as class, s.classes_id as class_id ".
									"from students as s,classes as c where c.id=s.classes_id and s.id=#id#n#",
		"students-update"		=>	"update students set name='#name#s#', slug='#slug#s#', description='#description#s#', classes_id=#classes_id#n# ".
									"where id=#id#n#",
		"students-create"		=>	"insert into students(name,slug,description,classes_id) ".
									"values('#name#s#','#slug#s#','#description#s#',#classes_id#n#)",				
		"students-delete"		=>	"delete from students where id=#id#n#",
		"quizzes-read"			=>	"select q.id as id, q.name as name, t.name as topic, q.description as description, ".
									"q.experience as experience, q.sticky as sticky, m.media_url as media ".
									"from quizzes as q,topics as t, media as m ".
									"where q.topic_id=t.id and q.media_id=m.id",
		"quizzes-read-id"		=>	"select q.id as id, q.name as name, t.name as topic, q.description as description, ".
									"q.experience as experience, q.sticky as sticky, m.media_url as media ".
									"from quizzes as q,topics as t, media as m ".
									"where q.topic_id=t.id and q.media_id=m.id and q.id=#id#n#",
		"quizzes-delete"		=>	"delete from quizzes where id=#id#n#",
		"questions-read"		=>	"select * from questions as que, quizzes as qui ".
									"where que.quiz_id=qui.id and qui.id=#id#n#"
	);
	private $validate = ["e" => "is_email"];
	private $sanitize = ["s" => "sanitize_text_field","n" => "intval","e" => "sanitize_email"];
	private $escape = ["s" => "escape_html","n" => "escape_html","e" => "escape_html", "t" => "escape_textarea"];

	public function __construct() {
		add_action('wp_ajax_quiz',[$this,'quiz']);
		add_action('wp_ajax_nopriv_quiz',[$this,'quiz']);

		/*$this->db = mysqli_connect(
			"localhost",
			"root", 
			"root", 
			"first_semester_project"
		);*/
		$this->db = new wpdb(
			"root", 
			"root", 
			"first_semester_project",
			"localhost"
		);
	}

	function quiz() {
		$nameformat = $this->get_name_and_format($this->sqls[$_GET["cmd"]]);
		$return = $this->populate_sql($this->sqls[$_GET["cmd"]],$nameformat);
		if ( $return["validate"]["check"] > 0 ) wp_send_json($return["validate"]);

		$result = $this->db->get_results($return["sql"]);
		$id = $this->db->insert_id;
		//$this->db->query("insert into status(tekst) values('".str_replace("'","''",json_encode($result))."')");
		$dbout = [ "executed: " => ($result!=null), "id" => $id ]; 

		$dbout = array();
		if ( strpos($this->sqls[$_GET["cmd"]],"select ") !== false ) {
	    	foreach ( $result as $row ) {
	    		$line = array();
	    		foreach ( $row as $key => $value ) {
					if ( $escape[$format] ) $line[$key] = $escape[$format]($value);
					else $line[$key] = $value;
	    		}
	    		$dbout[] = $line;
	    	}	
	    }
		wp_send_json($dbout);

		/*$nameformat = $this->get_name_and_format($this->sqls[$_GET["cmd"]]);
		$return = $this->populate_sql($this->sqls[$_GET["cmd"]],$nameformat);
		if ( $return["validate"]["check"] > 0 ) wp_send_json($return["validate"]);

		$result = mysqli_query($this->db,$return["sql"]);
		$id = mysqli_insert_id ($this->db);
		$dbout = [ "executed: " => $result, "id" => $id ]; 

		$dbout = array();
		if ( strpos($this->sqls[$_GET["cmd"]],"select ") !== false ) {
	    	while ( $row = $result->fetch_assoc() ) {
	    		$line = array();
	    		foreach ( $row as $key => $value ) {
					if ( $escape[$format] ) $line[$key] = $escape[$format]($value);
					else $line[$key] = $value;
	    		}
	    		$dbout[] = $line;
	    	}	
	    }
		wp_send_json($dbout);*/
	}

	private function populate_sql($sql,$nameformat) {
		foreach ( $nameformat as $name => $format ) {
			$input = $_POST[$name];
			if ( $this->validate[$format] ) {
				$check = ( $this->validate[$format]($input) !== false ) ? 1 : 0;
				$status["check"] += ( 1 - $check );
				$status[$name] = $check;
			}
			$sql = str_replace("#".$name."#".$format."#",$this->sanitize[$format]($input),$sql);
		}		
		return ["sql" => $sql, "validate" => $status];
	}

	private function get_name_and_format($sql) {
		$output = array();
		preg_match_all("/#\w+#[a-z]{1}#/",$sql,$matches); 
		foreach ( $matches[0] as $match ) {
			$temp = explode("#",$match);
			$output[$temp[1]] = $temp[2];
		}
		return $output;
	}
}

new Ajax();