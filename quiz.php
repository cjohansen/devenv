<?php
/*
Plugin Name: devenv
Description: devenv
Version: 0.0.2
*/

class Quiz 
{
	public static $url;

	public function __construct() 
	{
		self::$url = plugins_url("devenv");

		register_activation_hook(__FILE__,[$this,"onactivation"]);
		register_deactivation_hook(__FILE__,[$this,"ondeactivation"]);

		require_once("pages.php");			
		require_once("ajax.php");			
		require_once("uploadmedia.php");			

	}

	public function onactivation() {
		$a = add_role("quizb","quizb",["read"=>true,"upload_files"=>true]);
		$role = get_role('quizb');
		$role->add_cap("quizb");
	}

	public function ondeactivation() {
		$role = get_role('quizb');
		$role->remove_cap("quizb");
		remove_role("quizb");
	}
}

new Quiz();




